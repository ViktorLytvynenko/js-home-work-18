let obj1 = {
    students: {
        name1: "Natasha",
        name2: "Karolina",
        name3: "Valera",
        name4: "Vera",
        name5: "Alex",
        name6: "Jeff",
        name7: "Sue",
        name8: "Scott",
        name9: "Joe",
        name10: "Jeremy",
    },
    lessons: {
        lesson1: "JS",
        lesson2: "HTML",
        lesson3: "CSS",
    },
    marks: [1, 2, 3, 4, 5],
    courses: [1, 2, 3, 4],
}



function cloneObj(obj) {
    let clone = {}
    for (let key in obj) {
        if (typeof obj[key] == "object") {
            clone[key] = cloneObj(obj[key])
        } else {
            clone[key] = obj[key]
        }
    }
    return clone
}

console.log(cloneObj(obj1))